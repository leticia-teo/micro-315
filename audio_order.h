#ifndef AUDIO_ORDER_H
#define AUDIO_ORDER_H

#define FFT_SIZE 1024

typedef enum {
  // 2 times FFT_SIZE because these arrays contain complex numbers (real +
  // imaginary)
  LEFT_CMPLX_INPUT = 0,
  RIGHT_CMPLX_INPUT,
  FRONT_CMPLX_INPUT,
  BACK_CMPLX_INPUT,
  // Arrays containing the computed magnitude of the complex numbers
  LEFT_OUTPUT,
  RIGHT_OUTPUT,
  FRONT_OUTPUT,
  BACK_OUTPUT
} BUFFER_NAME_t;

typedef enum { STOP, ORDER_ONE, ORDER_TWO, ORDER_THREE } command;
typedef enum { SILENCE, WORLD_OF_DREAM, THRILLER, AROUND_THE_WORLD } song;

void processAudioData(int16_t *data, uint16_t num_samples);

/*
 *	put the invoking thread into sleep until it can process the audio datas
 */
void wait_send_to_computer(void);

/*
 *	Returns the pointer to the BUFFER_NAME_t buffer asked
 */
float *get_audio_buffer_ptr(BUFFER_NAME_t name);

void search_highest_peak(uint8_t min_freq, uint8_t max_freq, float *data,
                         uint8_t *nb_occur_tbl);
void search_max_occur_index(uint8_t *nb_occur_tbl, int8_t *count_tbl);

void audio_analysis_start(void);

/*

Compares frequencies and returns the song heard

*/
song recognize_music(int8_t low_freq_index, int8_t mid_low_freq_index,
                     int8_t mid_high_freq_index, int8_t high_freq_index);

command get_audio_order(void);
void set_audio_order(song song);

#endif /* AUDIO_ORDER_H */
