/*
 * main.c
 *
 *
 * Code based on code of TP5 Noisy and modified by Leticia Tapia and Teo
 * Goddet for miniprojet of MT-315 class
 */

#include <arm_math.h>
#include <audio/microphone.h>
#include <camera/po8030.h>
#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <memory_protection.h>
#include <motors.h>
#include <spi_comm.h>
#include <usbcfg.h>

#include "main.h"

#include "audio_order.h"
#include "chameleon.h"
#include "instruction.h"
#include "lidar.h"
#include "mover.h"

static void serial_start(void) {
  static SerialConfig ser_cfg = {
      115200,
      0,
      0,
      0,
  };

  sdStart(&SD3, &ser_cfg); // UART3.
}

static void timer12_start(void) {
  // General Purpose Timer configuration
  // timer 12 is a 16 bit timer so we can measure time
  // to about 65ms with a 1Mhz counter
  static const GPTConfig gpt12cfg = {
      1000000, /* 1MHz timer clock in order to measure uS.*/
      NULL,    /* Timer callback.*/
      0, 0};

  gptStart(&GPTD12, &gpt12cfg);
  // let the timer count to max value
  gptStartContinuous(&GPTD12, 0xFFFF);
}

int main(void) {
  halInit();
  chSysInit();
  mpu_init();

  // starts the serial communication
  serial_start();
  // starts the USB communication
  usb_start();
  // starts timer 12
  timer12_start();
  // starts the camera
  dcmi_start();
  po8030_start();
  // inits the motors
  motors_init();
  // init spi comm
  spi_comm_start();

  // starts mic
  mic_start(&processAudioData);

  // audio order analysis
  audio_analysis_start();

  // starts the threads for the processing of the image
  process_image_start();

  // start the thread responsible for robot moves
  mover_start();

  // start the thread for lidar scans
  lidar_start();

  // wait to be sure everything is correctly initialized
  chThdSleepMilliseconds(500);

  // start thread for instruction switch
  instructions_start();

  /* Infinite loop. */
  while (1) {
    // everything is done in other threads (orchestration is done by
    // instruction.c)
    chThdSleepMilliseconds(1000);
  }
}

#define STACK_CHK_GUARD 0xe2dee396
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

void __stack_chk_fail(void) { chSysHalt("Stack smashing detected"); }
