#include <arm_math.h>
#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <motors.h>
#include <usbcfg.h>

#include "mover.h"

#define DEFAULT_SPEED 200

#define STEP_BY_MICROMETER 0.0076335

#define STEP_BY_MILLIDEG 0.00371

// TODO: Maybe Dynamic Allocation would be better
#define MOVE_STACK_SIZE 1000

#define ROTATION -1
#define LINEAR 1

int move_stack[MOVE_STACK_SIZE];
int move_cursor_pos = -1;

static BSEMAPHORE_DECL(mover_move_end, TRUE);
static SEMAPHORE_DECL(mover_lin_single_move_end, TRUE);
static SEMAPHORE_DECL(mover_rot_single_move_end, TRUE);

bool has_nextmove(void);
int get_next_move(void);
int step_from_dist(int dist);
int step_from_angle(int ang);

static int speed = DEFAULT_SPEED;

static bool is_moving = 0;

static int8_t type = ROTATION; // Pair move are rotations

static THD_WORKING_AREA(waMover, 1024);
static THD_FUNCTION(Mover, arg) {
  chRegSetThreadName(__FUNCTION__);
  (void)arg;

  systime_t time;

  int volatile target;
  bool right_ok;
  bool left_ok;
  int8_t dir;

  int single_move;

  while (1) {
    time = chVTGetSystemTime();
    if (has_nextmove()) {
      is_moving = 1;
      single_move = get_next_move();
      chprintf((BaseSequentialStream *)&SD3,
               "Next Move :  %d -> %d (type = %d) \n\r", move_cursor_pos + 1,
               single_move, type);

      if (type == ROTATION) {
        target = step_from_angle(single_move);
      } else {
        target = step_from_dist(single_move);
      }

      right_motor_set_pos(0);
      left_motor_set_pos(0);
      chprintf((BaseSequentialStream *)&SD3, "Init Position L%d R%d \n",
               left_motor_get_pos(), right_motor_get_pos());

      right_ok = 0;
      left_ok = 0;
      dir = (target > 0) ? 1 : -1;

      right_motor_set_speed(dir * speed);
      left_motor_set_speed(type * dir * speed);

      while (1) {
        chprintf((BaseSequentialStream *)&SD3, "Actual Position L%d R%d \n",
                 left_motor_get_pos(), right_motor_get_pos());

        if (dir * right_motor_get_pos() >= dir * target && right_ok != 1) {
          right_motor_set_speed(0);
          right_ok = 1;
        }

        if (type * dir * left_motor_get_pos() >= dir * target && left_ok != 1) {
          left_motor_set_speed(0);
          left_ok = 1;
        }

        if (right_ok && left_ok) {
          break;
        }

        chThdSleepUntilWindowed(time, time + MS2ST(50));
      }

      if (type == ROTATION) {
        chSemSignal(&mover_rot_single_move_end);
        type = LINEAR;
      } else {
        chSemSignal(&mover_lin_single_move_end);
        type = ROTATION;
      }

      if (!has_nextmove()) {
        is_moving = 0;
        chBSemSignal(&mover_move_end);
      }
    }
    chThdSleepUntilWindowed(time, time + MS2ST(50));
  }
}

void mover_start(void) {
  motors_init();
  chThdCreateStatic(waMover, sizeof(waMover), HIGHPRIO, Mover, NULL);
}

void wait_lin_single_move_end() { chSemWait(&mover_lin_single_move_end); }

void wait_rot_single_move_end() { chSemWait(&mover_rot_single_move_end); }

void wait_move_end() { chBSemWait(&mover_move_end); }

void mover_move(int move[], uint16_t move_number) {
  move_cursor_pos = -1; // dequeue previous vectors

  uint16_t parity = 0;
  if (move_number % 2 == 0) {
    move_cursor_pos = move_number - 1;
  } else {
    // ensure we have same number of rotation and translation
    parity = 1;
    move_stack[0] = 0;
    move_cursor_pos = move_number;
  }

  for (uint16_t i = 0; i < move_number; i++) {
    move_stack[i + parity] = move[move_number - i - 1];
  }

  type = ROTATION; // start by a rotation
  chSemReset(&mover_lin_single_move_end, 0);
  chSemReset(&mover_rot_single_move_end, 0);
}

bool mover_is_moving(void) { return is_moving; }
// dist is in micrometer, return a step number
int step_from_dist(int dist) {
  // need to compute the factor correctly
  return dist * STEP_BY_MICROMETER;
}

// ang is in milli-degré, return a step number
int step_from_angle(int ang) {
  // need to compute the factor correctly
  return ang * STEP_BY_MILLIDEG;
}

bool has_nextmove(void) { return move_cursor_pos >= 0; }

int get_next_move(void) {
  if (move_cursor_pos >= 0) {
    return move_stack[move_cursor_pos--];
  } else {
    return 0;
  }
}
