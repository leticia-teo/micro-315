#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <usbcfg.h>

#include "lidar_analyzer.h"

// TODO: maybe dynamic allocation should be used
#define MAX_MINIMAS_NUMBER 200

/*
 * those tables are global to avoid too much recalculations
 * their size is enough to be sure all minimas points
 * fits in any case.
 */
static uint16_t minimas_measurement_points[MAX_MINIMAS_NUMBER];
static int minimas_dists[MAX_MINIMAS_NUMBER];

// TODO: this repetition should be avoided
void swap32(int *a, int *b);
void swap16(uint16_t *a, uint16_t *b);

int calculate_closest_points(int lidar_points[], uint16_t points_number) {
  int prev = LIDAR_MAX_VALUE_FILTER + 1;

  uint8_t falling = 0;
  uint16_t obj = 0;

  for (uint16_t i = 0; i < points_number; i++) {
    if (lidar_points[i] < prev && lidar_points[i] > LIDAR_MIN_VALUE_FILTER &&
        lidar_points[i] < LIDAR_MAX_VALUE_FILTER) {
      falling = 1;
    } else if (falling) {
      falling = 0;
      minimas_dists[obj] = prev;
      minimas_measurement_points[obj] = i - 1;
      obj++;
    }
    prev = lidar_points[i];
  }

  /*
   * bubble sort algorithm inspired from
   * https://www.edureka.co/blog/sorting-algorithms-in-c/
   * adapted to sort two arrays
   * TODO: use a more efficient sort algorithm
   */
  int i, j;
  for (i = 0; i < obj - 1; i++) {
    for (j = 0; j < obj - i - 1; j++) {
      if (minimas_dists[j] > minimas_dists[j + 1]) {
        swap32(&minimas_dists[j], &minimas_dists[j + 1]);
        swap16(&minimas_measurement_points[j],
               &minimas_measurement_points[j + 1]);
      }
    }
  }
  return obj;
}

int get_nth_closest_dist(uint16_t rank) { return minimas_dists[rank - 1]; }

int get_nth_closest_measurement_point(uint16_t rank) {
  return minimas_measurement_points[rank - 1];
}

void swap32(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}

void swap16(uint16_t *a, uint16_t *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}
