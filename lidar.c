#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <usbcfg.h>

#include "lidar.h"

#include "VL53L0X.h"
#include "mover.h"

#define NUMBER_MEASURE_MEAN 4

static int measure[LIDAR_MEASUREMENT_NUMBER];
static int circ_move[2 * LIDAR_MEASUREMENT_NUMBER];

static BSEMAPHORE_DECL(start_lidar_scan, TRUE);
static BSEMAPHORE_DECL(lidar_scan_finished, TRUE);

static bool scan_ongoing = 0;

static THD_WORKING_AREA(waLidarMeasure, 256);
static THD_FUNCTION(lidarMeasure, arg) {
  chRegSetThreadName(__FUNCTION__);
  (void)arg;

  // prepare the move plan
  for (uint16_t i = 0; i < 2 * LIDAR_MEASUREMENT_NUMBER; i = i + 2) {
    circ_move[i] = LIDAR_SPACING;
    circ_move[i + 1] = 0;
  }

  while (1) {
    chBSemWait(&start_lidar_scan);
    scan_ongoing = 1;
    measure[0] = VL53L0X_get_dist_mm(); // initial measure before rotation

    for (uint16_t i = 1; i < LIDAR_MEASUREMENT_NUMBER; i++) {
      mover_move(circ_move, 2);
      wait_move_end();
      measure[i] = 0;
      for (int j = 0; j < NUMBER_MEASURE_MEAN; j++) {
        measure[i] = measure[i] + VL53L0X_get_dist_mm();
        chThdSleepMilliseconds(60);
      }
      measure[i] = measure[i] / NUMBER_MEASURE_MEAN;
      chprintf((BaseSequentialStream *)&SD3, "TOF # %d Angle %d Dist %d\r\n", i,
               i * LIDAR_SPACING, measure[i]);
    }

    scan_ongoing = 0;
    chBSemSignal(&lidar_scan_finished);
  }
}

void lidar_start(void) {
  VL53L0X_start();
  chThdCreateStatic(waLidarMeasure, sizeof(waLidarMeasure), NORMALPRIO,
                    lidarMeasure, NULL);
}

void lidar_start_measure(void) { chBSemSignal(&start_lidar_scan); }

int *lidar_wait_measure(void) {
  lidar_start_measure();
  chBSemWait(&lidar_scan_finished);
  return lidar_get_measure();
}

bool lidar_is_scan_ongoing(void) { return scan_ongoing; }

int *lidar_get_measure(void) { return measure; }
