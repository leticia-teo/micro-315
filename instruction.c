#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <usbcfg.h>

#include "instruction.h"

#include "VL53L0X.h"
#include "audio_order.h"
#include "chameleon.h"
#include "lidar.h"
#include "lidar_analyzer.h"
#include "mover.h"
#include "path_planner.h"

#define LIDAR_COOLDOWN_MS 3000

#define RED_MAX_DIST 1000000
#define RED_FOLLOW_DIST 100000
// we use the same value as the lidar by convenience
#define RED_SEARCH_ANGLE_SPACING LIDAR_SPACING

static THD_WORKING_AREA(order_thd_wa, 2048);

static THD_FUNCTION(order_thd, arg) {
  (void)arg;
  chRegSetThreadName(__FUNCTION__);

  systime_t time;

  systime_t last_lidar_time = 0;
  bool lidar_cooldown_ok = 1;

  uint32_t dist = 0;

  int move[100];
  int nb_move;

  int *lidar_points = NULL;
  while (1) {
    time = chVTGetSystemTime();
    switch (get_audio_order()) {
    case ORDER_ONE:
      /*
       * starts lidar and goes in a specific angle and distance of
       * the closest object
       */
      if (lidar_points == NULL && lidar_cooldown_ok == 1) {
        lidar_points = lidar_wait_measure();
        calculate_closest_points(lidar_points, LIDAR_MEASUREMENT_NUMBER);

        int move[100];
        int nb_move = calculate_move_for_target_polar(
            get_nth_closest_measurement_point(1) * LIDAR_SPACING,
            get_nth_closest_dist(1) * 1000 + ROBOT_RAYON, 60000, move);

        chprintf((BaseSequentialStream *)&SD3,
                 "Angle Target 1: %d, Dist Target 1: %d, \n\r",
                 get_nth_closest_measurement_point(1) * LIDAR_SPACING,
                 get_nth_closest_dist(1) * 1000 + ROBOT_RAYON);
        chprintf((BaseSequentialStream *)&SD3,
                 "Angle Target 2: %d, Dist Target 2: %d, \n\r",
                 get_nth_closest_measurement_point(2) * LIDAR_SPACING,
                 get_nth_closest_dist(2) * 1000 + ROBOT_RAYON);
        chprintf((BaseSequentialStream *)&SD3,
                 "Angle Target 3: %d, Dist Target 3: %d, \n\r",
                 get_nth_closest_measurement_point(3) * LIDAR_SPACING,
                 get_nth_closest_dist(3) * 1000 + ROBOT_RAYON);

        mover_move(move, nb_move);
        wait_move_end();

        lidar_cooldown_ok = 0;
        last_lidar_time = chVTGetSystemTime();
      }
      break;

    case ORDER_TWO:
      /*
       * find and follow any red target close enough
       */
      dist = VL53L0X_get_dist_mm() * 1000;

      if (get_main_color() == RED &&
          dist < RED_MAX_DIST) { // if we see red on the camera, follow it
        move[0] = 0;
        move[1] = dist - RED_FOLLOW_DIST;
      } else { // turn a bit to search for some red
        move[0] = -RED_SEARCH_ANGLE_SPACING;
        move[1] = 0;
      }

      nb_move = 2;
      mover_move(move, nb_move);
      wait_move_end();

      break;

    case ORDER_THREE:
      /*
       * If no other move is ongoing, it does an equilateral triangle.
       */
      if (!mover_is_moving()) {
        move[0] = 120000;
        move[1] = 150000;
        move[2] = 120000;
        move[3] = 150000;
        move[4] = 120000;
        move[5] = 150000;
        nb_move = 6;

        mover_move(move, nb_move);
        wait_move_end();
      }
      break;

    case STOP: // do nothing
      break;
    }

    if (lidar_cooldown_ok == 0 &&
        chVTGetSystemTime() >= (last_lidar_time + MS2ST(LIDAR_COOLDOWN_MS))) {
      lidar_cooldown_ok = 1;
      lidar_points = NULL;
    }

    chThdSleepUntilWindowed(time, time + MS2ST(300));
  }
}

void instructions_start(void) {
  chThdCreateStatic(order_thd_wa, sizeof(order_thd_wa), NORMALPRIO, order_thd,
                    NULL);
}
