#ifndef PATH_PLANNER_H
#define PATH_PLANNER_H

#define DECALAGE_ANGLE 220000

#define ROBOT_RAYON 35000
#define SAFETY_DIST 65000

uint16_t calculate_move_for_target(int x, int y, int robot_angle_at_target,
                                   int move[]);
uint16_t calculate_move_for_target_polar(int target_angle, int target_distance,
                                         int robot_angle_at_target, int move[]);

#endif
