/*
 * chameleon.c
 *
 *
 * Code based on code of TP4 CamReg and modified by Leticia Tapia and Teo
 * Goddet for miniprojet of MT-315 class
 */

#include <camera/dcmi_camera.h>
#include <camera/po8030.h>
#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <leds.h>
#include <selector.h>
#include <spi_comm.h>
#include <usbcfg.h>

#include "chameleon.h"

static color main_color = OTHER;

#define TOTAL_PXL 640
#define CENTER_OF_LINE 320
#define NB_PXL_MEAN 10
#define LINE_NUMBER 180

#define RED_GAIN 3.75
#define GREEN_GAIN 0.625
#define BLUE_GAIN 3.75

#define EXPOSURE 600

static BSEMAPHORE_DECL(image_ready_sem, TRUE);

static THD_WORKING_AREA(waCaptureImage, 256);
static THD_FUNCTION(CaptureImage, arg) {
  chRegSetThreadName(__FUNCTION__);
  (void)arg;

  /*
  Configuration of the camera
  */
  // takes pixels 0 to IMAGE_BUFFER_SIZE of the line 180 + 181 (minimum 2 lines
  // because reasons)
  po8030_advanced_config(FORMAT_RGB565, 0, LINE_NUMBER, IMAGE_BUFFER_SIZE, 2,
                         SUBSAMPLING_X1, SUBSAMPLING_X1);
  dcmi_enable_double_buffering();
  dcmi_set_capture_mode(CAPTURE_ONE_SHOT);
  dcmi_prepare();
  // auto-exposure and auto-white balance are disabled and set fixed values.
  po8030_set_rgb_gain(RED_GAIN, GREEN_GAIN, BLUE_GAIN);
  po8030_set_exposure(EXPOSURE, 0);

  while (1) {
    // configures contrast
    po8030_set_contrast(contrast_value());
    // starts a capture
    dcmi_capture_start();
    // waits for the capture to be done
    wait_image_ready();
    // signals an image has been captured
    chBSemSignal(&image_ready_sem);
  }
}

static THD_WORKING_AREA(waProcessImage, 1024);
static THD_FUNCTION(ProcessImage, arg) {
  chRegSetThreadName(__FUNCTION__);
  (void)arg;

  systime_t time;
  time = chVTGetSystemTime();

  uint8_t *img_buff_ptr;
  uint8_t image[IMAGE_BUFFER_SIZE] = {0};

  // mean's variables
  static int16_t meanGreen = 0;
  static int16_t meanRed = 0;
  static int16_t meanBlue = 0;

  while (1) {
    // waits until an image has been captured
    chBSemWait(&image_ready_sem);
    // gets the pointer to the array filled with the last image in RGB565
    img_buff_ptr = dcmi_get_last_image_ptr();

    // take blue color
    for (uint16_t i = 0; i < (2 * IMAGE_BUFFER_SIZE); i += 2) {
      image[i / 2] = 0;
      image[i / 2] = (uint8_t)img_buff_ptr[i + 1] & 0b00011111;
    }
    // do average of blue color of NB_PXL_MEAN points at the center of the line
    // and multiply by 2 for color calibration
    meanBlue = calc_color_average(NB_PXL_MEAN, image, CENTER_OF_LINE) * 2;

    // take red color
    for (uint16_t i = 0; i < (2 * IMAGE_BUFFER_SIZE); i += 2) {
      image[i / 2] = 0;
      // extracts first 5bits of the first byte
      // takes nothing from the second byte
      uint8_t temp = 0;
      temp = (uint8_t)img_buff_ptr[i] & 0xF8;
      image[i / 2] = (temp >> 3);
    }
    // do average of red color of NB_PXL_MEAN points at the center of the line
    // and and multiply by 2 for color calibration
    meanRed = calc_color_average(NB_PXL_MEAN, image, CENTER_OF_LINE) * 2;

    // take green color
    for (uint16_t i = 0; i < IMAGE_BUFFER_SIZE; i++) {
      image[i] = 0;
      uint8_t lsb = 0;
      uint8_t msb = 0;

      lsb = img_buff_ptr[2 * i + 1] & 0b11100000;
      msb = img_buff_ptr[2 * i] & 0b00000111;

      image[i] |= (lsb >> 5);
      image[i] |= (msb << 3);
    }
    // do average of green color of NB_PXL_MEAN points at the center of the line
    meanGreen = calc_color_average(NB_PXL_MEAN, image, CENTER_OF_LINE);

    // puts value in proportion of the max=255
    if (meanRed >= meanBlue && meanRed >= meanGreen && meanRed != 0) {
      meanBlue = (255 * meanBlue) / meanRed;
      meanGreen = (255 * meanGreen) / meanRed;
      meanRed = 255;
    } else if (meanGreen >= meanRed && meanGreen >= meanBlue &&
               meanGreen != 0) {
      meanBlue = (255 * meanBlue) / meanGreen;
      meanRed = (255 * meanRed / meanGreen);
      meanGreen = 255;
    } else if (meanBlue >= meanRed && meanBlue >= meanGreen && meanBlue != 0) {
      meanGreen = (255 * meanGreen) / meanBlue;
      meanRed = (255 * meanRed / meanBlue);
      meanBlue = 255;
    }

    // sets leds
    set_rgb_led(LED2, meanRed, meanGreen, meanBlue);
    set_rgb_led(LED4, meanRed, meanGreen, meanBlue);
    set_rgb_led(LED6, meanRed, meanGreen, meanBlue);
    set_rgb_led(LED8, meanRed, meanGreen, meanBlue);

    define_main_color(meanRed, meanGreen, meanBlue);

    chThdSleepUntilWindowed(time, time + MS2ST(100));
  }
}

// decides which color is the main color
void define_main_color(int16_t red_lvl, int16_t green_lvl, int16_t blue_lvl) {
  if (red_lvl == 255 && blue_lvl <= 230 && green_lvl <= 230) {
    main_color = RED;
  } else if (blue_lvl == 255 && red_lvl <= 200 && green_lvl <= 200) {
    main_color = BLUE;
  } else if (green_lvl == 255 && red_lvl <= 200 && blue_lvl <= 200) {
    main_color = GREEN;
  } else if (green_lvl >= 200 && red_lvl >= 200 && blue_lvl >= 200) {
    main_color = WHITE;
  } else {
    main_color = OTHER;
  }
}

color get_main_color(void) { return main_color; }

// calculate color average
int16_t calc_color_average(uint16_t totalValues, uint8_t *image,
                           uint16_t center) {
  int16_t meanVar = 0;
  for (uint16_t i = 0; i < totalValues; i++) {
    meanVar += image[center - (totalValues / 2) + i];
  }
  meanVar = (meanVar / totalValues);
  return meanVar;
}

// Selector sets the contrast value
int16_t contrast_value(void) {
  int16_t contrast_val = 0;

  switch (get_selector()) {
  case 0:
    contrast_val = 64;
    break;
  case 1:
    contrast_val = 70;
    break;
  case 2:
    contrast_val = 80;
    break;
  case 3:
    contrast_val = 90;
    break;
  case 4:
    contrast_val = 100;
    break;
  case 5:
    contrast_val = 110;
    break;
  case 6:
    contrast_val = 120;
    break;
  case 7:
    contrast_val = 0;
    break;
  case 8:
    contrast_val = 10;
    break;
  case 9:
    contrast_val = 20;
    break;
  case 10:
    contrast_val = 30;
    break;
  case 11:
    contrast_val = 40;
    break;
  case 12:
    contrast_val = 50;
    break;
  }
  chprintf((BaseSequentialStream *)&SD3, "Contrast: %d\n",
           contrast_val); // user can monitor contrast level
  return contrast_val;
}

void process_image_start(void) {
  chThdCreateStatic(waProcessImage, sizeof(waProcessImage), NORMALPRIO,
                    ProcessImage, NULL);
  chThdCreateStatic(waCaptureImage, sizeof(waCaptureImage), NORMALPRIO,
                    CaptureImage, NULL);
}
