#include <arm_math.h>
#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <memory_protection.h>
#include <usbcfg.h>

#include "path_planner.h"

#define FULL_CIRCLE 360000
#define HALF_CIRCLE FULL_CIRCLE / 2

int primary_measure(int angle);
double to_math_angle(int angle);
int from_math_angle(double angle);

/*
 * This function calculate the required moves to go to a position with a certain
 * angle @destination.
 * Given a map of the zone, it may in the future plan move avoiding
 * obstacles.
 * return the number of rotation + linear advance planned
 */
uint16_t calculate_move_for_target(int x, int y, int robot_angle_at_target,
                                   int move[]) {
  return calculate_move_for_target_polar(
      from_math_angle(atan((double)x / (double)y)), sqrt(pow(x, 2) + pow(y, 2)),
      robot_angle_at_target, move);
}

uint16_t calculate_move_for_target_polar(int target_angle, int target_distance,
                                         int robot_angle_at_target,
                                         int move[]) {
  uint16_t nb_move = 4;

  /*
   * the 100, 0.00001 and 0.01 multiplications
   * are needed to avoid calculation overflow
   * which is made on 32 bits
   */
  int b = 100 *
          sqrt(pow(0.01 * target_distance, 2) + pow(0.01 * DECALAGE_ANGLE, 2) -
               0.0001 * 2 * target_distance * DECALAGE_ANGLE *
                   cos(to_math_angle(robot_angle_at_target)));

  int alpha = from_math_angle(asin((double)DECALAGE_ANGLE / (double)b *
                                   sin(to_math_angle(robot_angle_at_target))));

  chprintf((BaseSequentialStream *)&SD3,
           "Angle Target : %d, target_distance Target : %d, Alpha Calc %d, "
           "Length Calc %d \n\r",
           target_angle, target_distance, alpha, b);

  move[0] = primary_measure(target_angle - alpha);
  move[1] = b;
  move[2] = primary_measure(robot_angle_at_target + alpha);
  move[3] = DECALAGE_ANGLE - SAFETY_DIST - ROBOT_RAYON;

  return nb_move;
}

int primary_measure(int angle) {
  angle = angle % FULL_CIRCLE;
  if (angle > HALF_CIRCLE) {
    return angle - FULL_CIRCLE;
  } else if (angle < -HALF_CIRCLE) {
    return angle + FULL_CIRCLE;
  } else {
    return angle;
  }
}

double to_math_angle(int angle) {
  return (double)angle * M_PI / (double)HALF_CIRCLE;
}

int from_math_angle(double angle) { return angle * HALF_CIRCLE / M_PI; }
