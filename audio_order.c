/*
 * audio_order.c
 *
 *
 * Code based on code of TP5 Noisy and modified by Leticia Tapia and Teo
 * Goddet for miniprojet of MT-315 class
 */

#include <arm_const_structs.h>
#include <arm_math.h>
#include <audio/microphone.h>
#include <ch.h>
#include <chprintf.h>
#include <hal.h>
#include <leds.h>
#include <motors.h>
#include <usbcfg.h>

#include "audio_order.h"

// semaphore
static BSEMAPHORE_DECL(sendToComputer_sem, TRUE);

// 2 times FFT_SIZE because these arrays contain complex numbers (real +
// imaginary)
static float micLeft_cmplx_input[2 * FFT_SIZE];

// Arrays containing the computed magnitude of the complex numbers
static float micLeft_output[FFT_SIZE];

#define MIN_VALUE_THRESHOLD 7500

// limits are set separately in case of different limit decision
#define MIN_FREQ_LOW 5       // 78Hz
#define MAX_FREQ_LOW 13      // 203Hz
#define MIN_FREQ_MID_LOW 13  // 203Hz
#define MAX_FREQ_MID_LOW 24  // 375Hz
#define MIN_FREQ_MID_HIGH 25 // 390Hz
#define MAX_FREQ_MID_HIGH 40 // 625HZ
#define MIN_FREQ_HIGH 40     // 625Hz
#define MAX_FREQ_HIGH 70     // 1093Hz

#define MIN_OCURR_LIMIT 15
#define RANGES_NUMBER 4 // we have 4 different ranges

static uint8_t occur_tbl_low[MAX_FREQ_HIGH] = {0};
static uint8_t occur_tbl_mid_low[MAX_FREQ_HIGH] = {0};
static uint8_t occur_tbl_mid_high[MAX_FREQ_HIGH] = {0};
static uint8_t occur_tbl_high[MAX_FREQ_HIGH] = {0};

static command sound_order = STOP;

static const int8_t world_of_dream_tb[] = {-1, 5,  6,  7,  8,  11, 13, 14,
                                           16, 19, 22, 25, 26, 29, 32, 34,
                                           39, 43, 49, 51, 52, 58, 65, 68};

static const int8_t thriller_tb[] = {-1, 6,  7,  18, 22, 33,
                                     36, 37, 43, 49, 55, 65};

static const int8_t around_the_world_tb[] = {-1, 6,  7,  13, 14,
                                             16, 25, 26, 39, 40};

#define TRUE 1
#define FALSE 0

#define SONG_COMPARISON(range)                                                 \
  match_##range = 0;                                                           \
  for (uint8_t i = 0; i < ((sizeof range##_tb) / (sizeof range##_tb[0]));      \
       i++) {                                                                  \
    if (low_freq_index == range##_tb[i]) {                                     \
      match_##range++;                                                         \
    }                                                                          \
    if (mid_low_freq_index == range##_tb[i]) {                                 \
      match_##range++;                                                         \
    }                                                                          \
    if (mid_high_freq_index == range##_tb[i]) {                                \
      match_##range++;                                                         \
    }                                                                          \
    if (high_freq_index == range##_tb[i]) {                                    \
      match_##range++;                                                         \
    }                                                                          \
  }

// search for the norm index with max number of occurrences for frequencies
#define SEARCH_MAX_OCCUR_INDEX(range)                                          \
  search_max_occur_index(occur_tbl_##range, count_tbl);                        \
  count = count_tbl[1];                                                        \
  if (count >= MIN_OCURR_LIMIT)                                                \
    range##_freq_index = count_tbl[0];                                         \
  else                                                                         \
    range##_freq_index = -1;

static THD_WORKING_AREA(audio_analysis_thd_wa, 256);

static THD_FUNCTION(audio_analysis_thd, arg) {
  (void)arg;
  chRegSetThreadName(__FUNCTION__);
  systime_t time;

  while (1) {
    time = chVTGetSystemTime();
    chprintf((BaseSequentialStream *)&SD3, " time: %d \n", time);

    int8_t max_occur_norm_index = -1;
    int8_t count = 0;
    int8_t count_tbl[2] = {max_occur_norm_index,
                           count}; // first index stores max_occur_norm_index
                                   // value and second index stores count

    int8_t low_freq_index = -1;
    int8_t mid_low_freq_index = -1;
    int8_t mid_high_freq_index = -1;
    int8_t high_freq_index = -1;

    // search for the norm index with max number of occurrences for each
    // frequencies range
    SEARCH_MAX_OCCUR_INDEX(low);
    SEARCH_MAX_OCCUR_INDEX(mid_low);
    SEARCH_MAX_OCCUR_INDEX(mid_high);
    SEARCH_MAX_OCCUR_INDEX(high);

    set_audio_order(recognize_music(low_freq_index, mid_low_freq_index,
                                    mid_high_freq_index, high_freq_index));
    chprintf((BaseSequentialStream *)&SD3, "frequencies [%d %d %d %d]",
             low_freq_index, mid_low_freq_index, mid_high_freq_index,
             high_freq_index);

    chThdSleepUntilWindowed(time, time + MS2ST(5000)); // thd every 5 seconds
  }
}

/*
 *	Function used to detect the highest values in a buffer
 */
void sound_remote(float *data) {
  // Search for the highest peak at every range of frequencies
  search_highest_peak(MIN_FREQ_LOW, MAX_FREQ_LOW, data, occur_tbl_low);
  search_highest_peak(MIN_FREQ_MID_LOW, MAX_FREQ_MID_LOW, data,
                      occur_tbl_mid_low);
  search_highest_peak(MIN_FREQ_MID_HIGH, MAX_FREQ_MID_HIGH, data,
                      occur_tbl_mid_high);
  search_highest_peak(MIN_FREQ_HIGH, MAX_FREQ_HIGH, data, occur_tbl_high);
}

void set_audio_order(song song) {
  if (song == WORLD_OF_DREAM) {
    sound_order = ORDER_ONE;
    set_body_led(0);
    clear_leds();
    set_led(LED1, 1);
  } else if (song == THRILLER) {
    sound_order = ORDER_TWO;
    set_body_led(0);
    clear_leds();
    set_led(LED3, 1);
  } else if (song == AROUND_THE_WORLD) {
    sound_order = ORDER_THREE;
    set_body_led(0);
    clear_leds();
    set_led(LED5, 1);
  } else {
    sound_order = STOP;
    clear_leds();
    set_body_led(1);
  }

  chprintf((BaseSequentialStream *)&SD3, "Order number:%d song: %d \n",
           sound_order, song);
}

void search_highest_peak(uint8_t min_freq, uint8_t max_freq, float *data,
                         uint8_t *nb_occur_tbl) {
  int16_t max_norm_index = -1;
  float max_norm = MIN_VALUE_THRESHOLD;

  for (uint16_t i = min_freq; i < max_freq; i++) {
    if (data[i] > MIN_VALUE_THRESHOLD) {
      if (data[i] > max_norm) {
        max_norm = data[i];
        max_norm_index = i;
      }
    }
  }
  // put number of occurrences of the highest peak
  if (max_norm_index != -1) {
    nb_occur_tbl[max_norm_index]++;
  }
}

void search_max_occur_index(uint8_t *nb_occur_tbl, int8_t *count_tbl) {
  count_tbl[0] = -1; // max_occur_norm_index =-1
  count_tbl[1] = 0;  // count = 0

  for (uint16_t k = 0; k < MAX_FREQ_HIGH; k++) {
    if (nb_occur_tbl[k] > count_tbl[1]) {
      count_tbl[1] = nb_occur_tbl[k];
      count_tbl[0] = k;
    }
    nb_occur_tbl[k] = 0;
  }
}

song recognize_music(int8_t low_freq_index, int8_t mid_low_freq_index,
                     int8_t mid_high_freq_index, int8_t high_freq_index) {
  song song = SILENCE;

  uint8_t match_world_of_dream = 0;
  uint8_t match_thriller = 0;
  uint8_t match_around_the_world = 0;

  SONG_COMPARISON(world_of_dream);
  SONG_COMPARISON(thriller);
  SONG_COMPARISON(around_the_world);

  // research order according to difficulty to find
  if (match_around_the_world == RANGES_NUMBER &&
      !(low_freq_index == -1 && mid_low_freq_index == -1)) {
    song = AROUND_THE_WORLD;
    chprintf((BaseSequentialStream *)&SD3, " AROUND THE WORLD song : %d \n",
             song);
  } else if (match_thriller == RANGES_NUMBER &&
             !(mid_low_freq_index == -1 && mid_high_freq_index == -1)) {
    song = THRILLER;
    chprintf((BaseSequentialStream *)&SD3, " THRILLER song : %d \n", song);
  } else if (match_world_of_dream == RANGES_NUMBER) {
    song = WORLD_OF_DREAM;
    chprintf((BaseSequentialStream *)&SD3, " WORLD OF DREAM song : %d \n",
             song);
  } else {
    song = SILENCE;
    chprintf((BaseSequentialStream *)&SD3, " SILENCE : %d \n", song);
  }

  // catch some silences that may looks like music
  if ((low_freq_index == -1 && mid_low_freq_index == -1 &&
       mid_high_freq_index == -1) ||
      (low_freq_index == -1 && mid_low_freq_index == -1 &&
       high_freq_index == -1) ||
      (low_freq_index == -1 && mid_high_freq_index == -1 &&
       high_freq_index == -1) ||
      (mid_low_freq_index == -1 && mid_high_freq_index == -1 &&
       high_freq_index == -1)) {
    song = SILENCE;
  }
  return song;
}

command get_audio_order(void) { return sound_order; }

void audio_analysis_start(void) {
  chThdCreateStatic(audio_analysis_thd_wa, sizeof(audio_analysis_thd_wa),
                    NORMALPRIO, audio_analysis_thd, NULL);
}

/*
 *	Callback called when the demodulation of the four microphones is done.
 *	We get 160 samples per mic every 10ms (16kHz)
 *
 *	params :
 *	int16_t *data			Buffer containing 4 times 160 samples.
 *the samples are sorted by micro so we have [micRight1, micLeft1, micBack1,
 *micFront1, micRight2, etc...] uint16_t num_samples	Tells how many data we
 *get in total (should always be 640)
 */

void processAudioData(int16_t *data, uint16_t num_samples) {
  /*
   *
   *	We get 160 samples per mic every 10ms
   *	So we fill the samples buffers to reach
   *	1024 samples, then we compute the FFTs.
   *
   */

  static uint16_t nb_samples = 0;

  // loop to fill the buffers
  for (uint16_t i = 0; i < num_samples; i += 4) {
    micLeft_cmplx_input[nb_samples] = (float)data[i + MIC_LEFT];
    nb_samples++;
    micLeft_cmplx_input[nb_samples] = 0;
    nb_samples++;

    // stop when buffer is full
    if (nb_samples >= (2 * FFT_SIZE)) {
      break;
    }
  }

  if (nb_samples >= (2 * FFT_SIZE)) {
    /*	FFT proccessing
     *
     *	This FFT function stores the results in the input buffer given.
     *	This is an "In Place" function.
     */
    arm_cfft_f32(&arm_cfft_sR_f32_len1024, micLeft_cmplx_input, 0, 1);

    /*	Magnitude processing
     *
     *	Computes the magnitude of the complex numbers and
     *	stores them in a buffer of FFT_SIZE because it only contains
     *	real numbers.
     *
     */
    arm_cmplx_mag_f32(micLeft_cmplx_input, micLeft_output, FFT_SIZE);

    nb_samples = 0;
    sound_remote(micLeft_output);
  }
}
