
#This is a template to build your own project with the e-puck2_main-processor folder as a library.
#Simply adapt the lines below to be able to compile

# Define project name here
PROJECT = micro-315

#Define path to the e-puck2_main-processor folder
GLOBAL_PATH = ./lib/e-puck2_main-processor

#Source files to include
CSRC += ./main.c \
		./chameleon.c \
		./audio_order.c \
        ./lidar.c \
        ./path_planner.c \
        ./mover.c \
        ./lidar_analyzer.c \
		./instruction.c \

#Header folders to include
INCDIR += 

#Jump to the main Makefile
include $(GLOBAL_PATH)/Makefile
