#ifndef LIDAR_H
#define LIDAR_H

/* CAUTION : this value can be freely choosen,
 * but there are rounding errors, that can remove up to 30° per scan
 * To have the less possible error (<1°), you need to chose between those values
 * [121, 133, 148, 166, 190, 267]
 * Formula is STEP_BY_MILLIDEG/n*360000 with n any int
 */
#define LIDAR_MEASUREMENT_NUMBER 166

#define LIDAR_SPACING 360000 / LIDAR_MEASUREMENT_NUMBER

void lidar_start(void);

void lidar_start_measure(void);

int *lidar_wait_measure(void);

int *lidar_get_measure(void);

bool lidar_is_scan_ongoing(void);

#endif
