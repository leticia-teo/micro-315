#ifndef LIDAR_ANALYZER_H
#define LIDAR_ANALYZER_H

#define SEPARATION_ANGLE 10000

#define LIDAR_MIN_VALUE_FILTER 40
#define LIDAR_MAX_VALUE_FILTER 6000

int calculate_closest_points(int lidar_points[], uint16_t points_number);

int get_nth_closest_measurement_point(uint16_t rank);
int get_nth_closest_dist(uint16_t rank);

#endif
