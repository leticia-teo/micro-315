#ifndef CHAMELEON_H
#define CHAMELEON_H

#define IMAGE_BUFFER_SIZE 640

typedef enum { OTHER, GREEN, BLUE, RED, WHITE } color;

void define_main_color(int16_t red_lvl, int16_t green_lvl, int16_t blue_lvl);
void process_image_start(void);
int16_t calc_color_average(uint16_t totalValues, uint8_t *image,
                           uint16_t center);
color get_main_color(void);
int16_t contrast_value(void);

#endif /* CHAMELEON_H */
