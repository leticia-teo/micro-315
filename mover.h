#ifndef MOVER_H
#define MOVER_H

void mover_start(void);
void mover_move(int move[], uint16_t move_number);
void wait_move_end(void);
void wait_lin_single_move_end(void);
void wait_rot_single_move_end(void);
bool mover_is_moving(void);

#endif
